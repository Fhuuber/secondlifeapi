using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SecondLife.Service;
using SecondLifeModel.Entities;

namespace SecondLifeApi.Controllers.Tests
{
    [TestClass]
    public class UserControllerTests
    {
        private UserCrudController _controller;
        private Mock<IService<User>> _userServiceMock;

        public UserControllerTests()
        {
            _userServiceMock = new Mock<IService<User>>();
            _controller = new UserCrudController(_userServiceMock.Object);
        }

        //Un GET User avec un id � 0 ne doit pas �tre r�alis�
        [TestMethod]
        public void Get_With0_ThenGetFromRepoIsNotCalled()
        {
            _controller.Get(0);
            _userServiceMock.Verify(x => x.Get(0), Times.Never());
        }

        //Un GET User avec un id inexistant doit renvoyer une erreur NoContent
        [TestMethod]
        public void Get_WithUnexistingId_ThenGetFromRepoReturnsNoContent()
        {
            User user = null;

            _userServiceMock.Setup(u => u.Get(It.IsAny<int>())).Returns(user);

            var user2 = _controller.Get(55);

            Assert.IsInstanceOfType(user2.Result, typeof(NoContentResult));
        }

        //Un GET User avec un id � 1 doit �tre r�alis�
        [TestMethod]
        public void Get_With1_ThenGetFromRepoIsCalledWith1()
        {
            _controller.Get(1);
            _userServiceMock.Verify(x => x.Get(1), Times.Once());
        }

        //La cr�ation d'un User sans param�tres doit �chouer
        [TestMethod]
        public void Add_WithNoParameters_ThenAddIsNotCalled()
        {
            _controller.Create(new User());
            _userServiceMock.Verify(x => x.Add(It.IsAny<User>()), Times.Never());
        }

        //La cr�ation d'un User avec des param�tres manquants doit �chouer
        [TestMethod]
        public void Add_WithMissingParameters_ThenAddIsNotCalled()
        {
            _controller.Create(new User()
            {
                Mail = "test@gmail.com",
                Username = "username",
                Password = "password",
                PhoneNumber = "0123456789"
            });

            _userServiceMock.Verify(x => x.Add(It.IsAny<User>()), Times.Never());
        }

        //La cr�ation d'un User avec tous les param�tres n�cessaires doit r�ussir
        [TestMethod]
        public void Add_WithAllParameters_ThenAddIsCalled()
        {
            _controller.Create(new User()
            {
                Mail = "test@gmail.com",
                Username = "username",
                Password = "password",
                PhoneNumber = "0123456789",
                StreetNumber = 3,
                Street = "rue des Potiers",
                City = "STRASBOURG",
                PostalCode = "67000"
            });

            _userServiceMock.Verify(x => x.Add(It.IsAny<User>()), Times.Once());
        }

        //La suppression d'un enregistrement en base de donn�es avec un ID inexistant doit �chouer
        [TestMethod]
        public void DeleteWithUnknownId_ThenDeleteIsNotOk()
        {
            _userServiceMock.Setup(u => u.Delete(It.IsAny<int>())).Returns(false);

            var deletedUser = _controller.Delete(55);

            Assert.IsInstanceOfType(deletedUser, typeof(NotFoundResult));
        }

        //La suppression d'un enregistrement en base de donn�es avec un ID existant doit r�ussir
        [TestMethod]
        public void DeleteWithExistingId_ThenDeleteIsOk()
        {
            _userServiceMock.Setup(x => x.Get(It.IsAny<int>())).Returns(new User());
            _userServiceMock.Setup(x => x.Delete(It.IsAny<int>())).Returns(true);

            var deletedUser = _controller.Delete(1);

            Assert.IsInstanceOfType(deletedUser, typeof(OkResult));
        }
    }
}