﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SecondLife.Repositories;
using SecondLife.Service;
using SecondLife.Service.Services;
using SecondLife.Service.Services.IServices;
using SecondLifeModel.Entities;
using System.Collections.Generic;

namespace SecondLife.Services.Tests
{
    [TestClass]
    public class OffersServiceTests
    {
        private OffersService _service;
        private Mock<IRepositorie<User>> _repo;
        private IConfiguration _config;

        private Mock<IValidator<User>> _validator;
        private Mock<IValidator<Product>> _productValidator;
        private Mock<IValidator<Offer>> _offerValidator;
        private Mock<IValidator<Proposal>> _proposalValidator;

        private Mock<IService<User>> _userService;
        private Mock<IService<Product>> _productService;
        private Mock<IService<Game>> _gameService;
        private Mock<IService<Opinion>> _opinionService;
        
        private Mock<IProposalService> _proposalService;
        private Mock<IAuthService> _authService;
        private Mock<IService<Offer>> _offerService;

        public User getBuyer() { return new User() { Id = 1, Mail = "user1@test.com", Username = "user1", Password = "password", PhoneNumber = "0606060606", StreetNumber = 12, Street = "rue de Paris", City = "STRASBOURG", PostalCode = "67000" }; }
        public User getBuyer2() { return new User() { Id = 3, Mail = "user3@test.com", Username = "user3", Password = "password", PhoneNumber = "0808080808", StreetNumber = 14, Street = "rue de Berlin", City = "PARIS", PostalCode = "78000" }; }
        public User getSeller() { return new User() { Id = 2, Mail = "user2@test.com", Username = "user2", Password = "password", PhoneNumber = "0707070707", StreetNumber = 13, Street = "rue de Strasbourg", City = "PARIS", PostalCode = "78000" }; }
        public Product getProduct() { return new Product() { Id = 1, Name = "Biomutant", PictureUrl = "url", Game = getGame(), Description = "Le jeu Biomutant" }; }
        public Proposal getProposal() { return new Proposal() { Id = 1, Offer = getOffer(), ProposalState = ProposalState.Waiting, ProposedPrice = 100, RequestingUser = getBuyer(), ProposedProducts = new List<Product>() { getProduct() } }; }
        public Proposal getProposal2() { return new Proposal() { Id = 2, Offer = getOffer2(), ProposalState = ProposalState.Waiting, ProposedPrice = 100, RequestingUser = getBuyer(), ProposedProducts = new List<Product>() { getProduct() } }; }
        public Proposal getProposal3() { return new Proposal() { Id = 3, Offer = getOffer2(), ProposalState = ProposalState.Waiting, ProposedPrice = 100, RequestingUser = getBuyer2(), ProposedProducts = new List<Product>() { getProduct() } }; }
        public Proposal getRefusedProposal() { return new Proposal() { Id = 4, Offer = getOffer2(), ProposalState = ProposalState.Refused, ProposedPrice = 100, RequestingUser = getBuyer2(), ProposedProducts = new List<Product>() { getProduct() } }; }
        public Proposal getAcceptedProposal() { return new Proposal() { Id = 5, Offer = getOffer2(), ProposalState = ProposalState.Accepted, ProposedPrice = 100, RequestingUser = getBuyer2(), ProposedProducts = new List<Product>() { getProduct() } }; }
        public Proposal getProposalNotLinkedToAnOffer() { return new Proposal() { Id = 4, ProposalState = ProposalState.Waiting, ProposedPrice = 100, RequestingUser = getBuyer2(), ProposedProducts = new List<Product>() { getProduct() } }; }
        public Game getGame() { return new Game() { Id = 1, Name = "Jeu", Score = 5, Description = "Description", PictureUrl = "http://www.google.fr" }; }
        public List<Product> getProducts() { return new List<Product>() { new Product() { Description = "description", Name = "", PictureUrl = "", Game = getGame() } }; }
        public Offer getOffer() { return new Offer() { Title = "Vend 1 jeu PS2 bon état", Description = "Lorem ipsum", BasePrice = 4, Products = getProducts(), Seller = getSeller() }; }
        public Offer getOfferCreated() { return new Offer() { Id = 1, Title = "Vend 1 jeu PS2 bon état", Description = "Lorem ipsum", BasePrice = 4, Products = getProducts(), Seller = getSeller() }; }
        public Offer getOfferWithBuyer() { return new Offer() { Title = "Vend 1 jeu PS2 bon état", Description = "Lorem ipsum", BasePrice = 4, Products = getProducts(), Seller = getSeller(), Buyer = getBuyer() }; }
        public Offer getOffer2() { return new Offer() { Title = "Vend 1 jeu PC très bon état", Description = "Lorem ipsum", BasePrice = 15, Products = getProducts(), Seller = getSeller() }; }

        public OffersServiceTests()
        {
            _repo = new Mock<IRepositorie<User>>();

            _config = new ConfigurationBuilder()
                .AddInMemoryCollection(new Dictionary<string, string> { 
                    { "Jwt:Key", "fakeKeyfakeKeyfakeKeyfakeKeyfakeKey" }, 
                    { "Jwt:Issuer", "fakeIssuerfakeIssuerfakeIssuerfakeIssuer" } 
                })
                .Build();

            _offerService = new Mock<IService<Offer>>();
            _productService = new Mock<IService<Product>>(); ;

            _gameService = new Mock<IService<Game>>();
            _opinionService = new Mock<IService<Opinion>>();
            _productValidator = new Mock<IValidator<Product>>();
            _offerValidator = new Mock<IValidator<Offer>>();
            _validator = new Mock<IValidator<User>>();
            _proposalValidator = new Mock<IValidator<Proposal>>();
            _userService = new Mock<IService<User>>();

            _proposalService = new Mock<IProposalService>();
            _authService = new Mock<IAuthService>();

            _service = new OffersService(_offerService.Object, _productService.Object, _proposalService.Object, _gameService.Object, _authService.Object, _offerValidator.Object, _productValidator.Object, _proposalValidator.Object, _opinionService.Object);
        }

        [TestMethod]
        public void CallCheckIfActiveProposal_WithoutProposalInOffer_ThenReturnsFalse()
        {
            _proposalService.Setup(x => x.GetOffersProposals(It.IsAny<Offer>())).Returns(new List<Proposal> { });

            var activeProposal = _service.CheckIfActiveProposal(getBuyer2(), getOffer2());

            Assert.IsFalse(activeProposal);
        }

        [TestMethod]
        public void CallCheckIfActiveProposal_WithOthersWaitingProposalForThisUser_ThenReturnsTrue()
        {
            _proposalService.Setup(x => x.GetOffersProposals(It.IsAny<Offer>())).Returns(new List<Proposal>() { getProposal2() });

            var activeProposal = _service.CheckIfActiveProposal(getBuyer(), getOffer2());

            Assert.IsTrue(activeProposal);
        }

        [TestMethod]
        public void CallCheckIfActiveProposal_WithOthersWaitingProposalButNotForThisUser_ThenReturnsFalse()
        {
            _proposalService.Setup(x => x.GetOffersProposals(It.IsAny<Offer>())).Returns(new List<Proposal>() { getProposal3() });

            var activeProposal = _service.CheckIfActiveProposal(getBuyer(), getOffer2());

            Assert.IsFalse(activeProposal);
        }

        [TestMethod]
        public void CallDeclineProposal_WithUnknownProposal_ThenReturnsNull()
        {
            _proposalService.Setup(x => x.GetById(It.IsAny<int>())).Returns((Proposal)null);

            var declinedProposal = _service.DeclineProposal(getBuyer(), 999);

            Assert.IsNull(declinedProposal);
        }

        [TestMethod]
        public void CallDeclineProposal_WithProposalNotLinkedToAnOffer_ThenReturnsNull()
        {
            _proposalService.Setup(x => x.GetById(It.IsAny<int>())).Returns(getProposalNotLinkedToAnOffer());

            var declinedProposal = _service.DeclineProposal(getBuyer(), 46);

            Assert.IsNull(declinedProposal);
        }

        [TestMethod]
        public void CallDeclineProposal_WithProposalLinkedToAnUnknownOffer_ThenReturnsNull()
        {
            _proposalService.Setup(x => x.GetById(It.IsAny<int>())).Returns(new Proposal() { Offer = new Offer() { Id = 999 } }) ;
            _offerService.Setup(x => x.Get(It.IsAny<int>())).Returns((Offer)null);

            var declinedProposal = _service.DeclineProposal(getBuyer(), 47);

            Assert.IsNull(declinedProposal);
        }

        [TestMethod]
        public void CallDeclineProposal_WithMalformedProposal_ThenReturnsNull()
        {
            _proposalService.Setup(x => x.GetById(It.IsAny<int>())).Returns(getProposal());
            _offerService.Setup(x => x.Get(It.IsAny<int>())).Returns(getOfferCreated());
            _proposalValidator.Setup(x => x.CanEdit(It.IsAny<Proposal>())).Returns(false);

            var declinedProposal = _service.DeclineProposal(getSeller(), 48);

            Assert.IsNull(declinedProposal);
        }

        [TestMethod]
        public void CallDeclineProposal_WithGoodParamsButNotCalledByTheSeller_ThenReturnsNull()
        {
            _proposalService.Setup(x => x.GetById(It.IsAny<int>())).Returns(getProposal());
            _offerService.Setup(x => x.Get(It.IsAny<int>())).Returns(getOfferCreated());
            _proposalValidator.Setup(x => x.CanEdit(It.IsAny<Proposal>())).Returns(true);

            var declinedProposal = _service.DeclineProposal(getBuyer(), 48);

            Assert.IsNull(declinedProposal);
        }

        [TestMethod]
        public void CallDeclineProposal_WithGoodParamsAndSellerCalling_ThenReturnsRefusedProposal()
        {
            _proposalService.Setup(x => x.GetById(It.IsAny<int>())).Returns(getProposal());
            _offerService.Setup(x => x.Get(It.IsAny<int>())).Returns(getOfferCreated());
            _proposalValidator.Setup(x => x.CanEdit(It.IsAny<Proposal>())).Returns(true);
            _proposalService.Setup(x => x.DeclineProposal(It.IsAny<Proposal>())).Returns(new Proposal() { ProposalState = ProposalState.Refused });

            var declinedProposal = _service.DeclineProposal(getSeller(), 48);

            Assert.AreEqual(ProposalState.Refused, declinedProposal.ProposalState);
        }

        [TestMethod]
        public void CallAcceptProposal_WithUnknownProposal_ThenReturnsNull()
        {
            _proposalService.Setup(x => x.GetById(It.IsAny<int>())).Returns((Proposal)null);

            var acceptedProposal = _service.AcceptProposal(getSeller(), 999, "");

            Assert.IsNull(acceptedProposal);
        }

        [TestMethod]
        public void CallAcceptProposal_WithoutBeingTheSellerOfTheOffer_ThenReturnsNull()
        {
            _proposalService.Setup(x => x.GetById(It.IsAny<int>())).Returns(getProposal());

            var acceptedProposal = _service.AcceptProposal(getBuyer(), 10, "Message d'acceptation de la proposition");

            Assert.IsNull(acceptedProposal);
        }

        [TestMethod]
        public void CallAcceptProposal_BeingTheSellerButWithMissingParams_ThenReturnsNull()
        {
            _proposalService.Setup(x => x.GetById(It.IsAny<int>())).Returns(getProposal());
            _proposalValidator.Setup(x => x.CanEdit(It.IsAny<Proposal>())).Returns(false);

            var acceptedProposal = _service.AcceptProposal(getSeller(), 10, "Message d'acceptation de la proposition");

            Assert.IsNull(acceptedProposal);
        }

        [TestMethod]
        public void CallAcceptProposal_BeingTheSellerAndWithGoodParams_ThenReturnsCreatedProposal()
        {
            _proposalService.Setup(x => x.GetById(It.IsAny<int>())).Returns(getProposal());
            _proposalValidator.Setup(x => x.CanEdit(It.IsAny<Proposal>())).Returns(true);
            _proposalService.Setup(x => x.AcceptProposal(It.IsAny<List<Proposal>>(), It.IsAny<int>(), It.IsAny<string>())).Returns(new Proposal() { ProposalState = ProposalState.Accepted });

            var acceptedProposal = _service.AcceptProposal(getSeller(), 10, "Message d'acceptation de la proposition");

            Assert.AreEqual(ProposalState.Accepted, acceptedProposal.ProposalState);
        }

        [TestMethod]
        public void CallMakeAProposal_WithNoOfferAttachedToTheProposal_ThenReturnsNull()
        {
            _offerService.Setup(x => x.Get(It.IsAny<int>())).Returns((Offer)null);

            var proposal = _service.MakeAProposal(getBuyer(), new Proposal());
            
            Assert.IsNull(proposal);
        }

        [TestMethod]
        public void CallCheckIfOpinionExists_WithNoOpinionOnTheOffer_ThenReturnsFalse()
        {
            _offerService.Setup(x => x.Get(It.IsAny<int>())).Returns(new Offer() { Opinion = null });

            var opinionExists = _service.CheckIfOpinionExists(getOffer());

            Assert.IsFalse(opinionExists);
        }

        [TestMethod]
        public void CallDeleteMyOffer_WithUnknownOfferId_ThenReturnsFalse()
        {
            _offerService.Setup(x => x.Get(It.IsAny<int>())).Returns((Offer)null);

            var isOfferDeleted = _service.DeleteMyOffer(getBuyer(), 999);

            Assert.IsFalse(isOfferDeleted);
        }

        [TestMethod]
        public void CallDeleteMyOffer_WithMalformedOffer_ThenReturnsFalse()
        {
            _offerService.Setup(x => x.Get(It.IsAny<int>())).Returns(new Offer() { Id = 1 });
            _offerValidator.Setup(x => x.CanDelete(It.IsAny<int>())).Returns(false);

            var isOfferDeleted = _service.DeleteMyOffer(getBuyer(), 999);

            Assert.IsFalse(isOfferDeleted);
        }

        [TestMethod]
        public void CallDeleteMyOffer_WithoutBeingTheOfferSeller_ThenReturnsFalse()
        {
            _offerService.Setup(x => x.Get(It.IsAny<int>())).Returns(getOfferCreated());
            _offerValidator.Setup(x => x.CanDelete(It.IsAny<int>())).Returns(true);

            var isOfferDeleted = _service.DeleteMyOffer(getBuyer(), 1);

            Assert.IsFalse(isOfferDeleted);
        }

        [TestMethod]
        public void CallDeleteMyOffer_BeingTheOfferSeller_ThenReturnsTrue()
        {
            _offerService.Setup(x => x.Get(It.IsAny<int>())).Returns(getOfferCreated());
            _offerValidator.Setup(x => x.CanDelete(It.IsAny<int>())).Returns(true);
            _offerService.Setup(x => x.Delete(It.IsAny<int>())).Returns(true);

            var isOfferDeleted = _service.DeleteMyOffer(getSeller(), 1);

            Assert.IsTrue(isOfferDeleted);
        }


        [TestMethod]
        public void CallCheckIfOpinionExists_WithAnOpinionOnTheOffer_ThenReturnsTrue()
        {
            _offerService.Setup(x => x.Get(It.IsAny<int>())).Returns(new Offer() { Opinion = new Opinion() { Id = 1 } });

            var opinionExists = _service.CheckIfOpinionExists(getOffer2());

            Assert.IsTrue(opinionExists);
        }

        [TestMethod]
        public void CallMakeAProposal_WithBuyerInOfferObject_ThenReturnsNull()
        {
            _offerService.Setup(x => x.Get(It.IsAny<int>())).Returns(getOfferWithBuyer());

            var proposal = _service.MakeAProposal(getBuyer(), getProposal());

            Assert.IsNull(proposal);
        }

        [TestMethod]
        public void CallMakeAProposal_WithSimilarRequestingUserAndSeller_ThenReturnsNull()
        {
            _offerService.Setup(x => x.Get(It.IsAny<int>())).Returns(getOffer());

            var proposal = _service.MakeAProposal(getSeller(), getProposal());

            Assert.IsNull(proposal);
        }

        [TestMethod]
        public void CallMakeAProposal_WithAnotherProposalAlreadyActive_ThenReturnsNull()
        {
            _offerService.Setup(x => x.Get(It.IsAny<int>())).Returns(getOffer2());
            _proposalService.Setup(x => x.GetOffersProposals(It.IsAny<Offer>())).Returns(new List<Proposal>() { getProposal() } );

            var proposal = _service.MakeAProposal(getBuyer(), getProposal());

            Assert.IsNull(proposal);
        }

        [TestMethod]
        public void CallMakeAProposal_WithAMalformedProduct_ThenReturnsNull()
        {
            _offerService.Setup(x => x.Get(It.IsAny<int>())).Returns(getOffer2());
            _proposalService.Setup(x => x.GetOffersProposals(It.IsAny<Offer>())).Returns(new List<Proposal>() { getProposal() });
            _productValidator.Setup(x => x.CanAdd(It.IsAny<Product>())).Returns(false);

            var proposal = _service.MakeAProposal(getBuyer(), getProposal());

            Assert.IsNull(proposal);
        }

        [TestMethod]
        public void CallMakeAProposal_WithAMalformedProposal_ThenReturnsNull()
        {
            _offerService.Setup(x => x.Get(It.IsAny<int>())).Returns(getOffer2());
            _proposalService.Setup(x => x.GetOffersProposals(It.IsAny<Offer>())).Returns(new List<Proposal>() { getProposal() });
            _productValidator.Setup(x => x.CanAdd(It.IsAny<Product>())).Returns(true);
            _productService.Setup(x => x.Add(It.IsAny<Product>())).Returns(getProduct());
            _proposalValidator.Setup(x => x.CanAdd(It.IsAny<Proposal>())).Returns(false);

            var proposal = _service.MakeAProposal(getBuyer(), getProposal());

            Assert.IsNull(proposal);
        }

        [TestMethod]
        public void CallMakeAProposal_WithGoodParams_ThenReturnsProposal()
        {
            _offerService.Setup(x => x.Get(It.IsAny<int>())).Returns(getOffer2());
            _proposalService.Setup(x => x.GetOffersProposals(It.IsAny<Offer>())).Returns(new List<Proposal>() { getProposal2() });
            _productValidator.Setup(x => x.CanAdd(It.IsAny<Product>())).Returns(true);
            _productService.Setup(x => x.Add(It.IsAny<Product>())).Returns(getProduct());
            _proposalValidator.Setup(x => x.CanAdd(It.IsAny<Proposal>())).Returns(true);
            _proposalService.Setup(x => x.Create(It.IsAny<Proposal>())).Returns(getProposal3());

            var proposal = _service.MakeAProposal(getBuyer2(), getProposal3());

            Assert.IsNotNull(proposal);
        }

        [TestMethod]
        public void CallMakeAProposal_WithASecondProposalButTheFirstHasAlreadyBeenRefused_ThenReturnsProposal()
        {
            _offerService.Setup(x => x.Get(It.IsAny<int>())).Returns(getOffer2());
            _proposalService.Setup(x => x.GetOffersProposals(It.IsAny<Offer>())).Returns(new List<Proposal>() { getRefusedProposal() });
            _productValidator.Setup(x => x.CanAdd(It.IsAny<Product>())).Returns(true);
            _productService.Setup(x => x.Add(It.IsAny<Product>())).Returns(getProduct());
            _proposalValidator.Setup(x => x.CanAdd(It.IsAny<Proposal>())).Returns(true);
            _proposalService.Setup(x => x.Create(It.IsAny<Proposal>())).Returns(getProposal3());

            var proposal = _service.MakeAProposal(getBuyer2(), getProposal3());

            Assert.IsNotNull(proposal);
        }

        [TestMethod]
        public void CallCreateAnOffer_WithoutProducts_ThenReturnsNull()
        {
            var offer = _service.CreateAnOffer(new Offer(), getSeller());

            Assert.IsNull(offer);
        }

        [TestMethod]
        public void CallCreateAnOffer_WithoutMalformedProduct_ThenReturnsNull()
        {
            _gameService.Setup(x => x.Get(It.IsAny<int>())).Returns(getGame());
            _productValidator.Setup(x => x.CanAdd(It.IsAny<Product>())).Returns(false);

            var offer = _service.CreateAnOffer(getOffer(), getSeller());

            Assert.IsNull(offer);
        }

        [TestMethod]
        public void CallCreateAnOffer_WithoutMalformedOffer_ThenReturnsNull()
        {
            _gameService.Setup(x => x.Get(It.IsAny<int>())).Returns(getGame());
            _productValidator.Setup(x => x.CanAdd(It.IsAny<Product>())).Returns(true);
            _offerValidator.Setup(x => x.CanAdd(It.IsAny<Offer>())).Returns(false);

            var offer = _service.CreateAnOffer(getOffer(), getSeller());

            Assert.IsNull(offer);
        }

        [TestMethod]
        public void CallCreateAnOffer_WithoutProperOffer_ThenReturnsNull()
        {
            _gameService.Setup(x => x.Get(It.IsAny<int>())).Returns(getGame());
            _productValidator.Setup(x => x.CanAdd(It.IsAny<Product>())).Returns(true);
            _offerValidator.Setup(x => x.CanAdd(It.IsAny<Offer>())).Returns(true);
            _offerService.Setup(x => x.Add(It.IsAny<Offer>())).Returns(getOfferCreated());

            var offer = _service.CreateAnOffer(getOffer(), getSeller());

            Assert.IsNotNull(offer.Id);
        }
    }
}