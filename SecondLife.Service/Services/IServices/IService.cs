﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.AspNetCore.JsonPatch;

namespace SecondLife.Service
{
    // public interface IProposalSate : IService<ProposalState>{}
    public interface IService<T> where T : class
    {
        List<T> All(Expression<Func<T, bool>> condition = null);
        T Get(int serviceGet);
        T One();
        T Add(T serviceAdd);
        void Update(T serviceUpdate);
        bool Delete(int serviceDelete);
        bool Exists(T serviceExists);
        T Patch(int id, JsonPatchDocument<T> document);
    }
}