﻿using SecondLife.Repositories;
using SecondLifeModel.Entities;

namespace SecondLife.Service.Services.Validator
{
    public class GameValidator : IValidator<Game>
    {
        private IRepositorie<Game> _repo;

        public bool CanAdd(Game game)
        {
            if (game.Name != null && game.Description != null && game.PictureUrl != null)
                return true;

            return false;
        }

        public bool CanDelete(int id)
        {
            if (_repo.Exists(_repo.Get(id)))
                return true;

            return false;
        }

        public bool CanEdit(Game game)
        {
            if (_repo.Exists(_repo.Get(game.Id)))
                return true;

            return false;
        }

        public bool CanRead(int id)
        {
            if (_repo.Exists(_repo.Get(id)))
                return true;

            return false;
        }
    }
}