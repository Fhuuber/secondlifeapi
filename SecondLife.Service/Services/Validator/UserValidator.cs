﻿using SecondLife.Repositories;
using SecondLifeModel.Entities;

namespace SecondLife.Service.Services.Validator
{
    public class UserValidator : IValidator<User>
    {
        private IRepositorie<User> _repo;

        public bool CanAdd(User user)
        {
            if (!_repo.Exists(user))
                if (user.Mail != null && 
                    user.Username != null && 
                    user.Password != null && 
                    user.PhoneNumber != null && 
                    user.StreetNumber > 0 &&
                    user.Street != null && 
                    user.City != null && 
                    user.PostalCode != null)
                return true;

            return false;
        }

        public bool CanDelete(int id)
        {
            if (_repo.Exists(_repo.Get(id)))
                return true;

            return false;
        }

        public bool CanEdit(User user)
        {
            if (_repo.Exists(_repo.Get(user.Id)))
                return true;

            return false;
        }

        public bool CanRead(int id)
        {
            if (_repo.Exists(_repo.Get(id)))
                return true;

            return false;
        }
    }
}