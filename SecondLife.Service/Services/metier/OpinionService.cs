﻿using SecondLife.Service.Services.IServices;
using SecondLifeModel.Entities;

namespace SecondLife.Service.Services.metier
{
    public class OpinionService : IOpinionService
    {
        private IService<Opinion> _service;
        private IOfferService _offerService;

        public OpinionService(IService<Opinion> service, IOfferService offerService)
        {
            _service = service;
            _offerService = offerService;
        }

        public Opinion GetById(int id)
        {
            return _service.Get(id);
        }
    }
}