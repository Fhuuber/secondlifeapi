﻿using SecondLife.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecondLife.Service
{
    public class Validator<T> : IValidator<T>
    {
        public bool CanAdd(T canAdd)
        {
            return true;
        }

        public bool CanDelete(int canDelete)
        {
            return true;
        }

        public bool CanEdit(T canEdit)
        {
            return true;
        }

        public bool CanRead(int canRead)
        {
            return true;
        }
    }
}
