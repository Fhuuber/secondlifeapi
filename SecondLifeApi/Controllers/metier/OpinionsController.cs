﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SecondLife.Service.Services.IServices;
using SecondLifeModel.Entities;

namespace SecondLifeApi.Controllers.metier
{
    [ApiController, Route("api/[controller]"), Authorize]
    public class OpinionsController : ControllerBase
    {
        private IOpinionService _service;
        private IAuthService _authService;

        public OpinionsController(IOpinionService service, IAuthService authService)
        {
            _service = service;
            _authService = authService;
        }

        [HttpGet("{id}")]
        public IActionResult GetOpinion(int id)
        {
            Opinion opinion = _service.GetById(id);

            if (opinion != null)
                return Ok(opinion);

            return NotFound();
        }
    }
}