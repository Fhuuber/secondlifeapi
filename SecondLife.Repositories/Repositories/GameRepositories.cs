using System.Linq;
using SecondLifeModel;
using SecondLifeModel.Entities;
using System.Linq.Expressions;
using System;

namespace SecondLife.Repositories.Repositories
{
    public class GameRepositories : GenericRepository<Game>, IGameRepositories
    {
        private readonly SecondLifeDbContext _context;
        public new bool Exists(Game gameExists)
        {
            var dbProduct = _context.Games.FirstOrDefault(x => x.Name == gameExists.Name);
            return gameExists != null;
        }

        public GameRepositories(SecondLifeDbContext context) : base(context)
        {
            _context = context;
        }

        public new Game One()
        {
            return _context.Games.FirstOrDefault();
        }

        public new Game Add(Game game)
        {
            _context.Add(game);
            _context.SaveChanges();
            return game;

        }

        public new void Update(Game updateGame)
        {
            _context.Attach(updateGame);
            _context.SaveChanges();
        }

        public new Game Get(int gameId)
        {
            return _context.Games.Find(gameId);
        }

        public new bool Delete(int gameDelete)
        {
            var obj = Get(gameDelete);
            if (obj == null)
                return false;

            _context.Games.Remove(obj);
            _context.SaveChanges();
            return Get(gameDelete) == null;
        }

        public new Game Find(Expression<Func<Game, bool>> condition)
        {
            throw new NotImplementedException();
        }
    }
}