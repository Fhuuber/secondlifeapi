﻿using SecondLifeModel.Entities;

namespace SecondLife.Repositories
{
    public interface IOpinionRepositories : IRepositorie<Opinion> { }
}