﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SecondLifeModel.Entities
{
    /// <summary>
    /// Représente un vendeur/acheteur (tous les utilisateurs ont le même statut)
    /// </summary>
    public class User
    {
        [Key]
        public int Id { get; set; }

        public string Mail { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string PhoneNumber { get; set; }

        public int StreetNumber { get; set; }

        public string Street { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }

        //Liste de l'ensemble des offres vendu par utilisateur
        [JsonIgnore]
        public ICollection<Offer> SelledOffers { get; set; }

        //Liste de l'ensemble des offres acheté par l'utilisateur
        [JsonIgnore]
        public ICollection<Offer> BoughtOffers { get; set; }
    }
}